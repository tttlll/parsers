import info_about_number as ian
import tv_program as tvp
import proxy_pars as pp
import kino_poisk as kp

def test_not_empty():
    assert ian.handler_data(ian.get_value('9036544932')) != ''
    assert tvp.handler_data(tvp.get_value()) != ''
    assert pp.handler_data(pp.get_value()) != []
    assert kp.handler_data(tvp.get_value()) != ''


def test_type():
    assert type(ian.handler_data(ian.get_value('9036544932'))) == type('')
    assert type(tvp.handler_data(tvp.get_value())) == type('')
    assert type(pp.handler_data(pp.get_value())) == type([])
    assert type(kp.handler_data(kp.get_value())) == type('')


def test_error():
    assert ian.handler_data(ian.get_value('TEST')) == 'Error'
