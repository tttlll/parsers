import requests
from bs4 import BeautifulSoup


def get_value():

    url='https://tv.mail.ru/moskva/'
    r=requests.get(url)
    r=r.text
    return r


def handler_data(r):
    soup = BeautifulSoup(r, 'html.parser')
    name = soup.find_all("span", {'class': 'p-programms__item__name__link'})
    t_time= soup.find_all("span", {'class': 'p-programms__item__time'})
    title=soup.find_all("a", {'class': 'p-channels__item__info__title__link'})
    len_name = len(name)
    message = ''
    for i in range(len_name):
        if i % 5 == 0:
            message=message+('\n\n'+str(title[i//5].text))+'\n'
        message=message+(str(name[i].text)+ " "+str(t_time[i].text)+'\n')
    return message
