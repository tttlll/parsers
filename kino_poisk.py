import requests
from bs4 import BeautifulSoup


def get_value():
    url = 'https://www.kinopoisk.ru/popular/'
    r = requests.get(url)
    r = r.text
    return r


def handler_data(r):
    soup = BeautifulSoup(r, 'html.parser')
    soup = soup.find_all('div', {'class': ['el', 'el even']})
    num = 1
    message = ''
    for i in soup:

        message = '{} {} {} \n'.format(message, num, i.text.split('\n')[2])
        num += 1
    return message



