import requests
from bs4 import BeautifulSoup


def get_value():
    # Oh lol, if not to use user-agent, hh.ru hides data

    user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'
    url = 'https://nn.hh.ru/search/vacancy?text=&area=66'
    r = requests.get(url, headers={'User-Agent': user_agent})
    r = r.text
    return r


def handler_data(r):

    message = ''
    soup = BeautifulSoup(r, 'html.parser')
    about=soup.find_all('div',{'class':'vacancy-serp-item__info'})
    len_about=len(about)

    for i in range(0,len_about,2):
        message = '{} {} \n {}\n -------------------------------\n\n'.format(message,about[i].text,about[i+1].text)
    return message
