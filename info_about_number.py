import requests
from typing import AnyStr
from bs4 import BeautifulSoup


def get_value(phone) -> AnyStr:
    if len(phone) != 10 or not phone.isdigit():
        return 'not_number'
    url = 'http://tel-spravka.ru/tel/{}.html'.format(phone)
    r = requests.get(url)
    r = r.text
    return r


def handler_data(data) -> AnyStr:
    if data == 'not_number':
        return 'Error'
    message = ''
    soup = BeautifulSoup(data, 'html.parser')
    soup = soup.find_all('div')
    soup = soup[9]
    soup = soup.find_all('b')[:-1]
    for i in soup:
        message = message+i.text+'\n'
    return message
