import requests
from bs4 import BeautifulSoup


def get_value():
    user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'
    url = 'https://free.proxy-sale.com/'
    r = requests.get(url,headers={'User-Agent': user_agent},timeout=2)
    r = r.text
    return r


def handler_data(r):
    array = []
    soup = BeautifulSoup(r, 'html.parser',)
    name = soup.find_all('td', {'class': 'ip'}, 'a')
    for i in name:
        array.append(i.text.strip('\n').split('\t')[0])
    return array
